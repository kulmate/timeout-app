# README #

This application simulates a long response time to a given request.
Call get localhost:5000/{number_of_second_it_should_process_response} and wait for reposnse.
For example:
- Get localhost:5000/5 will answer after 5 seconds.
- Get localhost:5000/30 will answer after 30 seconds.

## Boot

- yarn start  
or even:
- docker-compose up -d --scale app=3  
Docker-compose adding nginx reverse-proxy in between.