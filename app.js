const express = require('express')
const safeStringify = require('fast-safe-stringify')
const app = express()
const port = 5000

function timeout(seconds) {
    return new Promise(resolve => setTimeout(resolve, seconds * 1000));
}

app.get('/*', async (req, res) => {
  await timeout(req.params[0])
  res.send(safeStringify(req.params))
})

app.listen(port, async () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
